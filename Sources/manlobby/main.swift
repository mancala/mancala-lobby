import Foundation
import SwiftOnSockets

public func runLobby() {
    
    do {
        try LobbyServiceConfig.parseArgsAndShare()
    } catch {
        print("ERROR: \(error)\n")
        print("Usage: [-port <external_lobby_port>] [-gs <internal_game_server_port>]\n")
        exit(-1)
    }
    
    do {
        print("[+] Starting Lobby")
        LobbyFactory.shared.createMatchMakerImpl = { MancalaMatchMaker() }
        let server = try LobbyService()
        try server.start()
        
        RunLoop.current.run()
    } catch (let error) {
        print("error=\(error)")
    }
    
    print("[+] Exiting Lobby")
}

runLobby()
