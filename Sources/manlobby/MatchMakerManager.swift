//
//  MatchMakerManager.swift
//  manlobby
//
//  Created by Michael Sanford on 5/19/17.
//
//

import Foundation
import NetworkingCore
import GamingCloudCore
import Dispatch

class MatchMakerManager {
    
    private let domain = "MatchMakerManager"
    private let matchMaker = LobbyFactory.shared.createMatchMaker()
    private let matchTimer: DispatchSourceTimer
    private let queue = DispatchQueue(label: "MatchMakerManagerQueue")
    private let responseQueue = DispatchQueue(label: "MatchMakerManager.responseQueue")
    private unowned let service: LobbyService

    func processCreateGameRequest(_ message: Message, fromClient client: ClientToken) {
        matchMaker.processCreateGameRequest(message, fromClient: client)
    }
    
    func removeGameRequest(for client: ClientToken) {
        matchMaker.removeGameRequest(for: client)
    }
    
    init(service: LobbyService) {
        self.service = service
        matchTimer = DispatchSource.makeTimerSource(flags: [], queue: queue)
        
        matchTimer.setEventHandler { [weak self] in
            guard let strongSelf = self, let requestMessage = strongSelf.matchMaker.makeMatchesAndCreateGameServerNewGameRequest(withTime: Date.now) else { return }
            ActivityMonitor.shared.reserveNewGames(with: requestMessage, completionQueue: strongSelf.responseQueue) { result in
                guard let result = result else {
                    log(.warning, strongSelf.domain, "failed to get create game response")
                    return
                }
                let clientGameResponses = strongSelf.matchMaker.buildCreateGameResponses(withRequest: requestMessage, response: result.0, gameServerAddress: result.1)
                service.sendClientGameResponses(clientGameResponses)
            }
        }
        // 1 time a second
        matchTimer.schedule(deadline: .now(), repeating: .seconds(1), leeway: .milliseconds(50))
        
        queue.async {
            self.matchTimer.resume()
        }
    }
}
