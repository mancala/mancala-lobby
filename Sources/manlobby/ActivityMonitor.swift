//
//  ActivityMonitor.swift
//  manlobby
//
//  Created by Michael Sanford on 5/17/17.
//
//

import Foundation
import SaaS
import NetworkingCore
import MancalaCloudCore
import GamingCloudCore
import SwiftOnSockets
import Dispatch

class ActivityMonitor {
    static let shared = ActivityMonitor()
        
    fileprivate struct GameServerInfo {
        let connectAddress: IPAddress
        var numberOfGames: Int
    }
    
    fileprivate struct BatchGameResponseCallbackInfo {
        let queue: DispatchQueue
        let callback: ((CreateGamesResponse, IPAddress)?) -> ()
    }
    
    fileprivate let domain = "ActivityMonitor"
    private let queue = DispatchQueue(label: "ActivityMonitorQueue")
    private let service: EventBasedService
    fileprivate var gameServerInfos: [ClientToken: GameServerInfo] = [:]
    fileprivate var newGameRequests: [UUID: BatchGameResponseCallbackInfo] = [:]
    fileprivate var mruGameServerInfos: [ClientToken: Date] = [:]
    
    init() {
        service = try! EventBasedService(tcpPort: LobbyServiceConfig.shared.gameServerPort, name: "ActivityMonitor", callbackQueue: queue)
        service.delegate = self
    }
    
    func start() throws {
        try service.start()
    }
    
    var gameServerForNewGames: ClientToken? {
        guard let firstGameServerInfo = gameServerInfos.first else { return nil }
        guard gameServerInfos.count > 1 else { return firstGameServerInfo.key }
        let zeroGameClientTokens: [ClientToken] = gameServerInfos.reduce([]) { result, info in
            guard info.value.numberOfGames == 0 else { return result }
            return result + [info.key]
        }
        guard zeroGameClientTokens.isEmpty else {
            guard let firstClientToken = zeroGameClientTokens.first else { return nil }
            guard zeroGameClientTokens.count > 1 else { return firstClientToken }
            guard let firstDate = mruGameServerInfos[firstClientToken] else { return firstClientToken}
            var bestClientToken: ClientToken = firstClientToken
            var mostDistantDate: Date = firstDate
            for token in zeroGameClientTokens {
                guard let date = mruGameServerInfos[token] else { return token }
                guard date.timeIntervalSince1970 < mostDistantDate.timeIntervalSince1970 else { continue }
                bestClientToken = token
                mostDistantDate = date
            }
            return bestClientToken
        }
        return gameServerInfos.reduce(firstGameServerInfo) { $1.value.numberOfGames < $0.value.numberOfGames ? $1 : $0 }.key
    }
    
    func reserveNewGames(with request: CreateGamesRequest,
                         completionQueue: DispatchQueue = DispatchQueue.main,
                         completion: @escaping ((CreateGamesResponse, IPAddress)?) -> ()) {
        // TODO: add support to timeout requests
        guard let gameServer = gameServerForNewGames else {
            completionQueue.async {
                completion(nil)
            }
            return
        }
        
        mruGameServerInfos[gameServer] = Date()
        queue.sync {
            newGameRequests[request.requestID] = BatchGameResponseCallbackInfo(queue: completionQueue, callback: completion)
            do {
                let message = Message(type: LobbyToGameServerMessageType.createGamesRequest.rawValue, payloadInfo: request)
                try service.send(message: message, toClient: gameServer)
            } catch {
                log(.warning, domain, "failed to send BatchCreateGameRequest message. error=\(error)")
                newGameRequests[request.requestID] = nil
                completionQueue.async {
                    completion(nil)
                }
                return
            }
        }
        
    }
}

extension ActivityMonitor: NetworkServiceDelegate {
    func service(_ service: NetworkService, didConnectToClient token: ClientToken) {
        log(.debug, domain, "ActivityMontior did receive connection")
    }
    
    func service(_ service: NetworkService, didDisconnectFromClient token: ClientToken, reason: DisconnectReason) {
        gameServerInfos[token] = nil
        mruGameServerInfos[token] = nil
        print("[+] Game server disonnected (port=\(token.remoteAddress.port))")
    }
    
    func service(_ service: NetworkService, didReceiveMessage message: Message, fromClient token: ClientToken) {
        guard let messageType = GameServerToLobbyMessageType(rawValue: message.type) else {
            log(.warning, domain, "received unknown message type in ActvityMonitor (type=\(message.type))")
            return
        }
        
        switch messageType {
        case .gameServerStatusUpdate:
            guard let payload = message.payload else { return }
            let unarchiver = ByteUnarchiver(archive: payload)
            guard let statusUpdateMessage = GameServerStatusUpdate(unarchiver: unarchiver) else { return }
            gameServerInfos[token]?.numberOfGames = statusUpdateMessage.numberOfGames
            print("[+] Game server update (port=\(token.remoteAddress.port), games=\(statusUpdateMessage.numberOfGames))")

        case .gameServerCheckin:
            guard let payload = message.payload else { return }
            let unarchiver = ByteUnarchiver(archive: payload)
            guard let checkinMessage = GameServerCheckin(unarchiver: unarchiver) else { return }
            gameServerInfos[token] = GameServerInfo(connectAddress: checkinMessage.connectionAddress, numberOfGames: 0)
            print("[+] Game server connected (port=\(token.remoteAddress.port))")
            
        case .createGameResponse:
            guard let payload = message.payload else { return }
            let unarchiver = ByteUnarchiver(archive: payload)
            guard let response = CreateGamesResponse(unarchiver: unarchiver),
                let callbackInfo = newGameRequests[response.requestID] else { return }
            newGameRequests[response.requestID] = nil
            guard let connectionAddress = gameServerInfos[token]?.connectAddress else {
                callbackInfo.queue.async {
                    callbackInfo.callback(nil)
                }
                return
            }
            callbackInfo.queue.async {
                callbackInfo.callback((response, connectionAddress))
            }
        }
    }
}
