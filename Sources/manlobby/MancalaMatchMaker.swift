//
//  MancalaMatchMaker.swift
//
//  Created by Michael Sanford on 5/19/17.
//
//

import Foundation
import NetworkingCore
import GamingCore
import MancalaCore
import MancalaLobbyCore
import MancalaCloudCore
import SwiftOnSockets
import GamingCloudCore
import Dispatch

class MancalaMatchMaker: MatchMakerProtocol {
    
    private let queue = DispatchQueue(label: "MancalaMatchMaker")
    
    fileprivate struct UnmatchedDictionary {
        private var requestInfos: [UUID: ClientRequestInfo] = [:]
        private var requestIDByClientToken: [ClientToken: UUID] = [:]
        private var requestQueue = [UUID]()
        
        mutating func add(_ requestInfo: ClientRequestInfo) {
            requestInfos[requestInfo.requestID] = requestInfo
            requestIDByClientToken[requestInfo.clientToken] = requestInfo.requestID
            requestQueue.append(requestInfo.requestID)
            assert(requestInfos.count == requestQueue.count, "internal umatchedDictionary error; add")
        }
        
        @discardableResult
        mutating func removeRequest(for requestID: UUID) -> ClientRequestInfo? {
            guard let index = requestQueue.index(of: requestID), let requestInfo = requestInfos[requestID] else { return nil }
            requestQueue.remove(at: index)
            requestInfos[requestID] = nil
            requestIDByClientToken[requestInfo.clientToken] = nil
            assert(requestInfos.count == requestQueue.count, "internal umatchedDictionary error; removeRequest")
            return requestInfo
        }
        
        @discardableResult
        mutating func removeRequest(for clientToken: ClientToken) -> ClientRequestInfo? {
            guard let requestID = requestIDByClientToken[clientToken] else { return nil }
            return removeRequest(for: requestID)
        }
        
        mutating func updateMatches() -> [(ClientRequestInfo, ClientRequestInfo?)] {
            assert(requestInfos.count == requestQueue.count, "internal umatchedDictionary error; updateMatches-pre")
            guard !requestQueue.isEmpty else { return [] }
            
            var result: [(ClientRequestInfo, ClientRequestInfo?)] = []
            
            var printDiagnostics: Bool = false
            // perform system commands if needed
            for requestItem in requestQueue.enumerated() {
                guard let requestInfo = requestInfos[requestItem.element] else { continue }
                switch requestInfo.type {
                case .healthChecker:
                    // health check
                    requestInfos[requestItem.element] = nil
                    requestQueue.remove(at: requestItem.offset)
                    result.append((requestInfo, nil))
                case .diagnostics:
                    // diagnostics
                    requestInfos[requestItem.element] = nil
                    requestQueue.remove(at: requestItem.offset)
                    printDiagnostics = true
                case .player:
                    break
                }
            }
            
            if printDiagnostics {
                print("requestQueue.count=\(requestQueue.count)")
            }
            
            if requestQueue.count % 2 == 1, let lastRequestID = requestQueue.last, let lastRequestInfo = requestInfos[lastRequestID] {
                if lastRequestInfo.received.timeIntervalSinceNow > 10  {
                    // add player and bot match
                    result.append((lastRequestInfo, nil))
                    requestQueue.removeLast()
                    requestInfos[lastRequestID] = nil
                    requestIDByClientToken[lastRequestInfo.clientToken] = nil
                }
            }
            
            while requestQueue.count >= 2 {
                let p1ID = requestQueue.remove(at: 0)
                let p2ID = requestQueue.remove(at: 0)
                guard let requestInfo1 = requestInfos[p1ID], let requestInfo2 = requestInfos[p2ID] else { break }
                requestInfos[p1ID] = nil
                requestInfos[p2ID] = nil
                requestIDByClientToken[requestInfo1.clientToken] = nil
                requestIDByClientToken[requestInfo2.clientToken] = nil
                result.append((requestInfo1, requestInfo2))
            }
            assert(requestInfos.count == requestQueue.count, "internal umatchedDictionary error; updateMatches-post")
            return result
        }
        
    }
    
    fileprivate struct MatchedDictionary {
        struct BatchKey: Hashable {
            let requestID: UUID
            let gameID: Int
            let playerID: PlayerID
            
            var hashValue: Int {
                return requestID.hashValue + gameID + playerID
            }
        }
        
        private var requestByBatchRequest: [BatchKey: ClientRequestInfo] = [:]
        private var requestByClientID: [UUID: BatchKey] = [:]
        
        mutating func set(requestInfo: ClientRequestInfo, for batchRequestID: UUID, batchGameID: Int, playerID: PlayerID) {
            let key = BatchKey(requestID: batchRequestID, gameID: batchGameID, playerID: playerID)
            requestByBatchRequest[key] = requestInfo
            requestByClientID[requestInfo.requestID] = key
        }
        
        func requestInfo(for batchRequestID: UUID, batchGameID: Int, playerID: PlayerID) -> ClientRequestInfo? {
            let key = BatchKey(requestID: batchRequestID, gameID: batchGameID, playerID: playerID)
            return requestByBatchRequest[key]
        }
        
        mutating func remove(with batchRequestID: UUID, batchGameID: Int, playerID: PlayerID) -> ClientRequestInfo? {
            let key = BatchKey(requestID: batchRequestID, gameID: batchGameID, playerID: playerID)
            guard let requestInfo = requestByBatchRequest[key] else { return nil }
            requestByClientID[requestInfo.requestID] = nil
            requestByBatchRequest[key] = nil
            return requestInfo
        }
        
        mutating func remove(with clientRequestID: UUID) {
            guard let key = requestByClientID[clientRequestID] else { return }
            requestByBatchRequest[key] = nil
            requestByClientID[clientRequestID] = nil
        }
    }

    fileprivate struct ClientRequestInfo {
        enum RequestType {
            case player
            case healthChecker
            case diagnostics
        }
        let requestID: UUID
        let clientToken: ClientToken
        let playerInfo: MancalaPlayerInfo
        let received: Timestamp64
        
        var type: RequestType {
            if playerInfo.playerName.hasPrefix("__fs5.health_checker") {
                return .healthChecker
            } else if playerInfo.playerName.hasPrefix("__fs5.diagnostics") {
                return .diagnostics
            } else {
                return .player
            }
        }
    }
    
    private var unmatchedInfos = UnmatchedDictionary()
    private var matchedInfos = MatchedDictionary()
    
    // cancel via clientRequestID; matched or unmatched
    
    func removeGameRequest(for clientToken: ClientToken) {
        queue.async { [weak self] in
            self?.unmatchedInfos.removeRequest(for: clientToken)
        }
    }
    
    func processCreateGameRequest(_ request: Message, fromClient client: ClientToken) {
        guard let payload = request.payload else { return }
        let unarchiver = ByteUnarchiver(archive: payload)
        guard let gameRequest = PlayerNewGameRequest(unarchiver: unarchiver) else {
            assert(false, "unable to unarchive PlayerNewGameRequest")
            return
        }
        queue.async { [weak self] in
            let requestInfo = ClientRequestInfo(requestID: gameRequest.identifier, clientToken: client, playerInfo: gameRequest.playerInfo, received: Date.now)
            self?.unmatchedInfos.add(requestInfo)
        }
    }
    
    func makeMatchesAndCreateGameServerNewGameRequest(withTime time: Timestamp64) -> CreateGamesRequest? {
        var matches: [(ClientRequestInfo, ClientRequestInfo?)] = []
        queue.sync { [weak self] in
            guard let strongSelf = self else { return }
            matches = strongSelf.unmatchedInfos.updateMatches()
        }
        
        guard !matches.isEmpty else { return nil }
        let gameServerRequestID = UUID()
        var createGameInfos: [CreateGameInfo] = []
        matches.enumerated().forEach { [weak self] in
            guard let strongSelf = self else { return }
            let batchGameID = createGameInfos.count
            strongSelf.matchedInfos.set(requestInfo: $0.element.0, for: gameServerRequestID, batchGameID: batchGameID, playerID: 0)
            let playerServer1Info = MancalaServerPlayerInfo(type: .human, info: $0.element.0.playerInfo)
            
            let playerServer2Info: MancalaServerPlayerInfo
            if let opponentInfo = $0.element.1 {
                strongSelf.matchedInfos.set(requestInfo: opponentInfo, for: gameServerRequestID, batchGameID: batchGameID, playerID: 1)
                playerServer2Info = MancalaServerPlayerInfo(type: .human, info: opponentInfo.playerInfo)
            } else {
                let botInfo = MancalaPlayerInfo(playerName: "Guest", gameboardID: "mancala.theme.wood2", points: 2123, numberOfGamesPlayed: 123)
                playerServer2Info = MancalaServerPlayerInfo(type: .bot(.intermediate), info: botInfo)
            }
            let gameStartupInfo = GameStartupInfo(player1Info: playerServer1Info, player2Info: playerServer2Info, startingLogic: .toggle)
            let gameStartupArchive = ByteArchiver.archive(for: gameStartupInfo)
            let createGameInfo = CreateGameInfo(numberOfPlayers: 2, gameStartupInfo: gameStartupArchive)
            createGameInfos.append(createGameInfo)
        }
        let createGameRequest = CreateGamesRequest(requestID: gameServerRequestID, createGameInfos: createGameInfos)
        return createGameRequest
    }
    
    func buildCreateGameResponses(withRequest request: CreateGamesRequest, response: CreateGamesResponse, gameServerAddress: IPAddress) -> [(ClientToken, ClientGameResponse)] {
        guard request.createGameInfos.count == response.createGameResults.count, request.requestID == response.requestID else {
            assert(false, "buildCreateGameResponses has big problem")
            return []
        }
        
        return zip(request.createGameInfos, response.createGameResults).enumerated().reduce([]) { result, entry in
            
            switch entry.element.1 {
            case .success(let gameClientInfo):
                
                let clientGameResponses: [(ClientToken, ClientGameResponse)] = gameClientInfo.gameTokens.compactMap {
                    let playerID = $0.key
                    let opponentPlayerID = playerID == 0 ? 1 : 0
                    let gameToken = $0.value
                    
                    let gameStartupInfoUnarchiver = ByteUnarchiver(archive: entry.element.0.gameStartupInfo)
                    guard let gameStartupInfo = GameStartupInfo(unarchiver: gameStartupInfoUnarchiver) else {
                        assert(false, "unable to unarchive GameStartupInfo")
                        return nil
                    }
                    
                    let unarchiver = ByteUnarchiver(archive: gameClientInfo.gameInfo)
                    guard let mancalaInfo = MancalaGameClientInfo(unarchiver: unarchiver) else {
                        assert(false, "unable to unarchive MancalaGameClientInfo")
                        return nil
                    }
                    guard let playerRequestInfo = self.matchedInfos.remove(with: request.requestID, batchGameID: entry.offset, playerID: playerID) else {
                        assert(false, "unable to find expected match info")
                        return nil
                    }

                    let opponentPlayerInfo = gameStartupInfo.playerInfo(forPlayerID: opponentPlayerID)
                    
                    let gameInfo = MancalaGameInfo(opponentInfo: opponentPlayerInfo, startingPlayer: mancalaInfo.startingPlayer)
                    let clientGameResponse = ClientGameResponse(
                        requestID: playerRequestInfo.requestID,
                        gameServerAddress: gameServerAddress,
                        gameToken: gameToken,
                        gameInfo: ByteArchiver.archive(for: gameInfo))
                    
                    let clientToken = playerRequestInfo.clientToken
                    return (clientToken, clientGameResponse)
                }
                return result + clientGameResponses
            case .failure(let errorText):
                var clientGameResponses: [(ClientToken, ClientGameResponse)] = []
                if let playerRequestInfo1 = self.matchedInfos.remove(with: request.requestID, batchGameID: entry.offset, playerID: 0) {
                    let clientGameResponse = ClientGameResponse(requestID: playerRequestInfo1.requestID, errorText: errorText)
                    clientGameResponses.append((playerRequestInfo1.clientToken, clientGameResponse))
                }
                if let playerRequestInfo2 = self.matchedInfos.remove(with: request.requestID, batchGameID: entry.offset, playerID: 1) {
                    let clientGameResponse = ClientGameResponse(requestID: playerRequestInfo2.requestID, errorText: errorText)
                    clientGameResponses.append((playerRequestInfo2.clientToken, clientGameResponse))
                }
                return result + clientGameResponses
            }
        }
        
//        guard let requestPayload = request.message.payload, let responsePayload = response.payload,
//            let batchRequest = BatchCreateGameRequest(unarchiver: ByteUnarchiver(archive: requestPayload)),
//            let batchResponse = BatchCreateGameResponse(unarchiver: ByteUnarchiver(archive: responsePayload)) else { return [] }
//        
//        let lobbyMessages: [ClientResponse] = batchResponse.createGameResponses.reduce([]) { result, gameResponseEntry in
//            let batchGameID = gameResponseEntry.key
//            let gameResponse = gameResponseEntry.value
//            guard let gameRequest = batchRequest.createGameInfos[batchGameID] else { return result }
//            guard let clientRequestInfo1 = self.matchedInfos.remove(with: batchResponse.requestID, batchGameID: batchGameID, playerID: 0) else { return result }
//            let newResponses: [ClientResponse]
//            let newGamePayload1 = LobbyDidFindNewGameMessage(
//                requestID: clientRequestInfo1.requestID,
//                gameServerAddress: gameServerAddress,
//                gameToken: gameResponse.player1GameToken,
//                opponentInfo: gameRequest.player2Info.info,
//                startingPlayer: gameResponse.startingPlayer)
//            let lobbyMessage1 = Message(type: LobbyToClientMessageType.createGameResponse.rawValue, payloadInfo: newGamePayload1)
//            let clientResponse1 = ClientResponse(clientToken: clientRequestInfo1.clientToken, message: lobbyMessage1)
//            if let player2GameToken = gameResponse.player2GameToken, let clientRequestInfo2 = self.matchedInfos.remove(with: batchResponse.requestID, batchGameID: batchGameID, playerID: 1) {
//                
//                let newGamePayload2 = LobbyDidFindNewGameMessage(
//                    requestID: clientRequestInfo2.requestID,
//                    gameServerAddress: gameServerAddress,
//                    gameToken: player2GameToken,
//                    opponentInfo: gameRequest.player1Info.info,
//                    startingPlayer: gameResponse.startingPlayer)
//                let lobbyMessage2 = Message(type: LobbyToClientMessageType.createGameResponse.rawValue, payloadInfo: newGamePayload2)
//                let clientResponse2 = ClientResponse(clientToken: clientRequestInfo2.clientToken, message: lobbyMessage2)
//                newResponses = [clientResponse1, clientResponse2]
//            } else {
//                newResponses = [clientResponse1]
//            }
//            return result + newResponses
//        }
//        
//        return lobbyMessages
    }
}

fileprivate func ==(lhs: MancalaMatchMaker.MatchedDictionary.BatchKey, rhs: MancalaMatchMaker.MatchedDictionary.BatchKey) -> Bool {
    return lhs.requestID == rhs.requestID && lhs.gameID == rhs.gameID && lhs.playerID == rhs.playerID
}
