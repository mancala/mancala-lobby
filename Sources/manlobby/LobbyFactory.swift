//
//  LobbyFactory.swift
//  manlobby
//
//  Created by Michael Sanford on 5/19/17.
//
//

import Foundation

class LobbyFactory {
    static let shared = LobbyFactory()
    
    func createMatchMaker() -> MatchMakerProtocol {
        return createMatchMakerImpl!()
    }
    
    var createMatchMakerImpl: (() -> MatchMakerProtocol)? = nil
}
