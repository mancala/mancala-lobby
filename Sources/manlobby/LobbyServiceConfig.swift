//
//  LobbyServiceConfig.swift
//  manlobby
//
//  Created by Michael Sanford on 5/22/17.
//
//

import Foundation
import NetworkingCore
import SwiftOnSockets

fileprivate let defaultLobbyPort: PortID = 8189
fileprivate let defaultGameServerPort: PortID = 8191

struct ParsedArgs {
    let lobbyPort: PortID?
    let gameServerPort: PortID?
}

enum CommandLineParseError: Error, CustomStringConvertible {
    case MissingLobbyPortValue
    case InvalidLobbyPortValue
    case MissingGameServerPortValue
    case InvalidGameServerPortValue

    var description: String {
        switch self {
        case .MissingLobbyPortValue: return "missing required lobby port value following -port option (i.e. 8088)"
        case .InvalidLobbyPortValue: return "invalid lobby port value (i.e. 8088)"
        case .MissingGameServerPortValue: return "missing required game server port value following -port option (i.e. 8088)"
        case .InvalidGameServerPortValue: return "invalid game server port value (i.e. 8088)"
        }
    }
}

class LobbyServiceConfig {
    static func parseArgsAndShare() throws {
        let args = try parseArgs()
        LobbyServiceConfig.shared = LobbyServiceConfig(config: args)
    }
    
    private static func parseArgs() throws -> ParsedArgs {
        let parsedLobbyPort: PortID?
        let parsedGameServerPort: PortID?

        let args = CommandLine.arguments
        // Parse for Lobby Port
        if let portIndex = args.index(of: "-port") {
            let portValueIndex = portIndex + 1
            guard portValueIndex < args.count else { throw CommandLineParseError.MissingLobbyPortValue }
            let portValue = args[portValueIndex]
            guard let portID = PortID(portValue) else { throw CommandLineParseError.InvalidLobbyPortValue }
            parsedLobbyPort = portID
        } else {
            print("[+] Missing lobby port. Defaults to port \(defaultLobbyPort).")
            parsedLobbyPort = nil
        }
        
        // Parse for Game Server Port
        if let portIndex = args.index(of: "-gs") {
            let portValueIndex = portIndex + 1
            guard portValueIndex < args.count else { throw CommandLineParseError.MissingGameServerPortValue }
            let portValue = args[portValueIndex]
            guard let portID = PortID(portValue) else { throw CommandLineParseError.InvalidGameServerPortValue }
            parsedGameServerPort = portID
        } else {
            print("[+] Missing game server port. Defaults to port \(defaultGameServerPort).")
            parsedGameServerPort = nil
        }

        return ParsedArgs(
            lobbyPort: parsedLobbyPort,
            gameServerPort: parsedGameServerPort
        )
    }
    
    static private(set) var shared: LobbyServiceConfig!

    init(config: ParsedArgs) {
        clientPort = config.lobbyPort ?? 8189
        gameServerPort = config.gameServerPort ?? 8191
    }
    
    let clientPort: PortID
    let gameServerPort: PortID
}
