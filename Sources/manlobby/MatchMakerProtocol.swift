//
//  MatchMakerProtocol.swift
//  manlobby
//
//  Created by Michael Sanford on 5/19/17.
//
//

import Foundation
import NetworkingCore
import SwiftOnSockets
import GamingCore
import GamingCloudCore

public typealias ResponseByGameServer = [IPAddress: Message]

struct ClientResponse {
    let clientToken: ClientToken
    let message: Message
}

protocol MatchMakerProtocol {
    func processCreateGameRequest(_ message: Message, fromClient client: ClientToken)
    func removeGameRequest(for client: ClientToken)
    
    func makeMatchesAndCreateGameServerNewGameRequest(withTime time: Timestamp64) -> CreateGamesRequest?
    func buildCreateGameResponses(withRequest request: CreateGamesRequest, response: CreateGamesResponse, gameServerAddress: IPAddress) -> [(ClientToken, ClientGameResponse)]
}
