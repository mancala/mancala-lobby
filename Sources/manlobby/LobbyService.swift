//
//  LobbyService.swift
//  manlobby
//
//  Created by Michael Sanford on 5/22/17.
//
//

import Foundation
import NetworkingCore
import SaaS
import GamingCore
import Dispatch

class LobbyService {
    fileprivate let domain = "LobbyService"
    let service: NetworkService
    var matchMaker: MatchMakerManager!
    let incomingQueue: DispatchQueue
    
    init() throws {
        incomingQueue = DispatchQueue(label: "LobbyService.incomingQueue")
        let service = try EventBasedService(tcpPort: LobbyServiceConfig.shared.clientPort, callbackQueue: incomingQueue)
        self.service = service
        self.matchMaker = MatchMakerManager(service: self)
        service.delegate = self
    }
    
    func start() throws {
        try ActivityMonitor.shared.start()
        try service.start()
    }
    
    func sendClientGameResponses(_ responses: [(ClientToken, ClientGameResponse)]) {
        for response in responses {
            let message = Message(type: LobbyToClientMessageType.createGameResponse.rawValue, payloadInfo: response.1)
            do {
                try service.send(message: message, toClient: response.0)
            } catch {
                log(.warning, domain, "failed to send newGameResponse; error=\(error) | client=\(response.0)")
            }
        }
    }
}

extension LobbyService: NetworkServiceDelegate {
    
    func service(_ service: NetworkService, didConnectToClient token: ClientToken) {
        log(.debug, domain, "LobbyService did receive connection")
    }
    
    func service(_ service: NetworkService, didDisconnectFromClient token: ClientToken, reason: DisconnectReason) {
        self.matchMaker.removeGameRequest(for: token)
    }
    
    func service(_ service: NetworkService, didReceiveMessage message: Message, fromClient token: ClientToken) {
        guard let type = ClientToLobbyMessageType(rawValue: message.type) else {
            assert(false, "invalid message type. execpting valid ClientToLobbyMessageType")
            return
        }
        
        switch type {
        case .createGameRequest:
            matchMaker.processCreateGameRequest(message, fromClient: token)
        case .cancelCreateGameRequest:
            break
        }
    }
}
