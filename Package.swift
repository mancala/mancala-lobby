// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "manlobby",
    dependencies: [
        .package(url: "git@gitlab.com:msanford1030/SaaS.git", from: "1.0.1"),
        .package(url: "git@gitlab.com:mancala/mancala-lobby-core.git", from: "1.1.2"),
        .package(url: "git@gitlab.com:mancala/gaming-cloud-core.git", from: "1.2.0"),
        .package(url: "git@gitlab.com:mancala/mancala-cloud-core.git", from: "1.1.0"),
    ],
    targets: [
        .target(
            name: "manlobby",
            dependencies: ["SaaS", "MancalaLobbyCore", "GamingCloudCore", "MancalaCloudCore"]),
    ]
)
